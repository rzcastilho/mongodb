#!/bin/bash

set -e

source /etc/environment

# Verify if MONGO_PATH_CONFIG_FILE exists, if not exists set default value
if [ -z ${MONGO_PATH_CONFIG_FILE+x} ]; then
	MONGO_PATH_CONFIG_FILE=$MONGO_DEFAULT_PATH_CONFIG_FILE
fi
export MONGO_PATH_CONFIG_FILE

DAEMON=mongod
ARGS="--config $MONGO_PATH_CONFIG_FILE"

# Verify if process is already running
if [ -e /var/run/$DAEMON.pid ]; then
	# Kill process and remove PID file
	PID=`cat /var/run/$DAEMON.pid`
  rm /var/run/$DAEMON.pid
	EXISTS=`ps -ef | awk '{ print $2; }' | grep -E "^$PID" | wc -l`
	if [ ${EXISTS} -ne 0 ]; then
		kill $PID
	fi
fi

# Start process and save PID
nohup $DAEMON $ARGS 1>/var/log/$DAEMON.out 2>/var/log/$DAEMON.err &
echo $! > /var/run/$DAEMON.pid

exit 0
